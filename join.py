#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 19:07:44 2020

@author: administrateur
"""

from multiprocessing import Process
import time

def f(i):

    time.sleep(0.02)
    print('hello world1 ', i)
    time.sleep(0.02)
    print('hello world2 ', i)
    
    time.sleep(0.3)
    print('hello world3 ', i)

if __name__ == '__main__':
    for num in range(10):
        p = Process(target=f, args=(num,))
        p.start()
        #p.join()