# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 11:41:08 2020

@author: Fred
"""
import time
from multiprocessing import Pool
import os

def f(x):
    #time.sleep(0.5)
    print("\n",os.getpid())
    return x*x

if __name__ == '__main__':
    with Pool(10) as p:
        print(p.map(f, range(300)))
        #res=p.apply_async(f, (20,))
        #print(res.get(timeout=1))