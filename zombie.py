#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 16:18:31 2020

@author: administrateur
"""

import os
import time


def pereFils():
      
      #Variable du père qui seront dupliquée dans le fils

      #Création du fils
      newpid = os.fork()

      if newpid == -1:
          print("Erreur de création")

      elif newpid == 0: # dans le fils
          #Variables modifiées par le fils donc dans son propre espace mémoire
          a=1             
          print('{0} {1}'.format('PID du fils ',os.getpid()))
          time.sleep(0.2)

      else: #newpid>0 -> dans le père
          print('{0} {1}'.format('PID du père ',os.getpid()))
          time.sleep(30)
    


if __name__=="__main__":
    pereFils()