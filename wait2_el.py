# import the os module of Python

import os
import time


def pereFils():
    newpid = os.fork()
    if newpid == -1:
        print("Erreur de création")

    elif newpid == 0:
        for i in range(0, 5):
            print("J'écris %d"%(i))
            time.sleep(2)
    else: 
        childProcExitInfo = os.wait()  

if __name__=="__main__":
    pereFils()

