# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 15:29:00 2020

@author: Fred
"""

from datetime import datetime
import time
import multiprocessing
from multiprocessing import Semaphore, Process


def proc(sem, interval):
    print("Le processus: %r, demande un jeton, le nombre de jetons disponible est: %r"
              % (multiprocessing.current_process().name, sem.get_value()))
    #time.sleep(1)
    sem.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton et utlise la resource, le nombre de jetons disponible est %r."
              % (multiprocessing.current_process().name, sem.get_value()))
 
        time.sleep(interval*10)
    finally:
        print("Le processus: %r, libére un jeton et n'utlise plus la resource."
              % (multiprocessing.current_process().name))
       
        sem.release()
       # print("Le nombre de jetons disponible est %r."
             # % (sem.get_value()))


if __name__ == '__main__':
    sem = Semaphore(1)    
    print("le nombre de jetons disponible dans le sémaphore est: %r" % sem.get_value())
    for i in range(3):
        Process(target = proc, args = (sem, i)).start()