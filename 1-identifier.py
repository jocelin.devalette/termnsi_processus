#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 16:18:30 2020

@author: administrateur
"""

import os

def identifierProcessus():
    
    #identification du processus instancié par ce programme
    print("Voici l'identité du processus instancié par le programme 1-identifier.py")
    print('PID : ',os.getpid())
    print('PID du père : ',os.getppid())
    print('Le propriétaire réel est : ', os.getuid())
    print('Le propriétaire effectif est : ', os.geteuid())
            
            
if __name__=="__main__":
    identifierProcessus()
