# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 15:29:00 2020

@author: Fred
"""

from datetime import datetime
import time
import multiprocessing
from multiprocessing import Semaphore, Process


def proc1(sem1, sem2):

    sem1.acquire()
    try:
        print("Le processus: %r, a pris un jeton dans sem et utlise la resource , le nombre de jetons disponible dans sem est %r."
              % (multiprocessing.current_process().name, sem1.get_value()))
        time.sleep(2)
    finally:
        print("Le processus: %r, libére un jeton dans sem et n'utlise plus la resource ."
              % (multiprocessing.current_process().name))
       
        sem1.release()
    print("Le processus: %r, demande un jeton dans sem pour accèder à la ressource , le nombre de jetons disponible dans sem est: %r"
              % (multiprocessing.current_process().name, sem2.get_value()))
    sem2.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem et utlise la resource , le nombre de jetons disponible dans sem est %r."
              % (multiprocessing.current_process().name, sem2.get_value()))
 
        time.sleep(3)
    finally:
        print("Le processus: %r, libére un jeton dans sem et n'utlise plus la resource ."
              % (multiprocessing.current_process().name))
       
        sem2.release()
        
def proc2(sem1,sem2):

    sem2.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem et utlise la resource , le nombre de jetons disponible dans sem est %r."
              % (multiprocessing.current_process().name, sem2.get_value()))
        time.sleep(5)
    finally:
        print("Le processus: %r, libére un jeton dans sem et n'utlise plus la resource ."
              % (multiprocessing.current_process().name))
       
        sem2.release()
    print("Le processus: %r, demande un jeton pour accèder à la ressource , le nombre de jetons dans sem disponible est: %r"
              % (multiprocessing.current_process().name, sem1.get_value()))

    sem1.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem et utlise la resource , le nombre de jetons disponible dans sem est %r."
              % (multiprocessing.current_process().name, sem1.get_value()))
 
        time.sleep(5)
    finally:
        print("Le processus: %r, libére un jeton dans sem et n'utlise plus la resource ."
              % (multiprocessing.current_process().name))
       
        sem1.release()



if __name__ == '__main__':
    sem1 = Semaphore(1) 
    sem2 = Semaphore(1)  
    print("le nombre de jetons disponible dans le sémaphore 1 est: %r" % sem1.get_value())
    print("le nombre de jetons disponible dans le sémaphore 2 est: %r" % sem2.get_value())

    Process(target = proc1, args = (sem1, sem2)).start()
    Process(target = proc2, args = (sem1, sem2)).start()