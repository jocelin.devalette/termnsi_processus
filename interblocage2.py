# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 15:29:00 2020

@author: Fred
"""

from datetime import datetime
import time
import multiprocessing
from multiprocessing import Semaphore, Process


def proc1(sem1, sem2):
    #print("Le processus: %r, demande un jeton pour accèder à la ressource 1, le nombre de jetons disponible est: %r"
     #         % (multiprocessing.current_process().name, sem1.get_value()))
    #time.sleep(1)
    sem1.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem1 et utlise la resource 1, le nombre de jetons disponible dans sem1 est %r."
              % (multiprocessing.current_process().name, sem1.get_value()))
        time.sleep(2)

       
        
        print("Le processus: %r, demande un jeton pour accèder à la ressource 2, le nombre de jetons disponible disponible dans sem2 est: %r"
                  % (multiprocessing.current_process().name, sem2.get_value()))
        #time.sleep(1)
        sem2.acquire()
        try:
            
            print("Le processus: %r, a pris un jeton dans sem2 et utlise la resource 2, le nombre de jetons disponible est %r."
                  % (multiprocessing.current_process().name, sem2.get_value()))
     
            time.sleep(3)
        finally:
            print("Le processus: %r, libére un jeton et n'utlise plus la resource 2."
                  % (multiprocessing.current_process().name))
           
            sem2.release()
    finally:
        print("Le processus: %r, utlise la resource 1 et à également besoin de la ressource 2."
              % (multiprocessing.current_process().name))
        sem1.release()
        
def proc2(sem1,sem2):

    sem2.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem2 et utlise la resource 2 et à également besoin de la ressource 1, le nombre de jetons disponible disponible dans sem1 est %r."
              % (multiprocessing.current_process().name, sem1.get_value()))
        time.sleep(5)

        sem1.acquire()
        try:
            
            print("Le processus: %r, a pris un jeton et utlise la resource 1, le nombre de jetons disponible est %r."
                  % (multiprocessing.current_process().name, sem1.get_value()))
     
            time.sleep(5)
        finally:
            print("Le processus: %r, libére un jeton et n'utlise plus la resource 1."
                  % (multiprocessing.current_process().name))
           
            sem1.release()
    finally:
        print("Le processus: %r, libére un jeton et n'utlise plus la resource 2."
              % (multiprocessing.current_process().name))
        sem2.release()



if __name__ == '__main__':
    sem1 = Semaphore(1) 
    sem2 = Semaphore(1)  
    print("le nombre de jetons disponible dans le sémaphore 1 est: %r" % sem1.get_value())
    print("le nombre de jetons disponible dans le sémaphore 2 est: %r" % sem2.get_value())
    #for i in range(3):
    Process(target = proc1, args = (sem1, sem2)).start()
    Process(target = proc2, args = (sem1, sem2)).start()